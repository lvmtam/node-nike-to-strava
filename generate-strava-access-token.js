const { prompt } = require("enquirer");
const fs = require("fs");
const strava = require("strava-v3");

const stravaConfigTemplate = "./strava_config";
const stravaConfig = "./data/strava_config";

async function generate() {
  try {
    fs.mkdirSync("data");
  } catch (e) {}

  let content = fs.readFileSync(stravaConfigTemplate);
  fs.writeFileSync(stravaConfig, content);

  content = fs.readFileSync(stravaConfig);
  let config = JSON.parse(content);
  config.client_id = "45385";
  config.client_secret = "5cf458aba39aa37b2086ba8cd85d8fb0ab064c37";
  config.redirect_uri = "http://localhost";
  config.access_token = "";

  fs.writeFileSync(stravaConfig, JSON.stringify(config));

  // Generates the url to have full access
  strava.config(config);
  const url = strava.oauth.getRequestAccessURL({
    scope: "activity:write"
  });
  // We have to grab the code manually in the browser and then copy/paste it into strava_config as "access_token"
  console.log("Connect to the following url and copy the code:\n" + url + "\n");
  return prompt({
    type: "input",
    name: "code",
    message:
      "What is your Enter the code obtained from previous strava url (the code parameter in redirection url)"
  }).then(({ code }) => {
    if (!code) {
      console.log("no code provided");
      process.exit();
    }

    return strava.oauth
      .getToken(code)
      .then(result => {
        // We update the access token in strava conf file
        if (result.access_token === undefined) {
          throw "Problem with provided code: " + JSON.stringify(result);
        }

        config.access_token = result.access_token;
        fs.writeFileSync(stravaConfig, JSON.stringify(config));
        console.log("Saved access_token to", stravaConfig);
        return config.access_token
      })
      .catch(e => {
        console.log("Error.", e);
      });
  });
}

module.exports = generate;
