const axios = require('axios');

async function fetchActivities(token) {
    const d = new Date()
    d.setHours(-24 * 24)
    // console.log('▶ fetchActivities from date:', d.toISOString())

    const url = `https://api.nike.com/sport/v3/me/activities/after_time/${d.valueOf()}`
    const response = await axios({
        method: 'GET',
        headers: { 'Authorization': `Bearer ${token}` },
        url,
    })
    // console.log('▶ fetchActivities response:', response.status, response.statusText)
    if (response.statusText !== 'OK') return {}

    return response.data
}

async function fetchActivityDetail(token, activity_uuid) {
    const url = `https://api.nike.com/sport/v3/me/activity/${activity_uuid}?metrics=ALL`
    const response = await axios({
        method: 'GET',
        headers: { 'Authorization': `Bearer ${token}` },
        url,
    })
    // console.log('▶ fetchActivityDetail response:', response.status, response.statusText)
    if (response.statusText !== 'OK') {
        // console.log('▶ fetchActivityDetail response:', response)
        return {}
    }
    return response.data
}

function fetchRuns(token) {
    return fetchActivities(token).then(async (response) => {
        const { activities } = response
        // console.log(`▶ found ${activities.length} activities`)
        if (activities.length === 0) {
            // console.log('ℹ️ not found any ACTIVITIES.')
            return []
        }
    
        const runs = activities
        .filter(({type, tags}) => type === 'run' && tags['location'] === 'outdoors')
        .map(({tags, id, start_epoch_ms, active_duration_ms, summaries}) => ({
            id: id,
            title: tags['com.nike.name'],
            date: new Date(start_epoch_ms),
            distance: summaries.find(s => s.metric === 'distance').value,
            duration: active_duration_ms
        }))
        .sort((a, b) => {
            if (a.date > b.date) return -1
            if (a.date < b.date) return 1
            if (a.date = b.date) return 0
        })
    
        // console.log(`▶ found ${runs.length} runs`)
        if (runs.length === 0) {
            // console.log('ℹ️ not found any RUN from your Watch.')
            return []
        }
        return runs
    })
}

async function testTokenValid(token) {
    const d = new Date()
    d.setHours(-24)
    const url = `https://api.nike.com/sport/v3/me/activities/after_time/${d.valueOf()}`
    return axios({
        method: 'GET',
        headers: { 'Authorization': `Bearer ${token}` },
        url,
    })
    .then(response => response.statusText === 'OK')
    .catch(err => {
        // console.log('testTokenValid. err:', err.message)
        return false
    })
}

module.exports = {
    fetchActivities,
    fetchActivityDetail,
    fetchRuns,
    testTokenValid,
}