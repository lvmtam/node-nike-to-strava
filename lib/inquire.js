const { AutoComplete, prompt } = require('enquirer')
const { millisToMinutesAndSeconds } = require('./util')

function askChoiceActivity(activities) {
    const choices = activities.map((activity) => {
        const { title, date, id, distance, duration } = activity
        const distanceFix = Math.round((distance + Number.EPSILON) * 100) / 100
        const durationFix = millisToMinutesAndSeconds(duration)
        const dateString = date.toLocaleDateString()
        const infos = []
        infos.push(title)
        infos.push(`${dateString}`)
        infos.push(`${distanceFix} km, ${durationFix}`)
        infos.push(id)
        return {
            name: infos.join('\t|\t'),
            value: activity.id,
        }
    })
    const prompt = new AutoComplete({
        name: 'activity',
        message: 'Pick your activity to convert:',
        choices,
    })
    return prompt.run()
}

function askNikeToken() {
    console.log('')
    console.log('Please follow this article to get Nike+ Run Club access token (Authentication: Beaber <token>)')
    console.log('https://yasoob.me/posts/nike-run-club-data-visualization/')
    console.log('')
    
    return prompt({
        type: 'input',
        name: 'token',
        message: 'Enter your nike bearer access token.',
        validate: (input) => {
            if (input && input.length) {
                return true
            }
            return false
        }
    }).then((answer) => answer.token)
}

module.exports = {
    askChoiceActivity,
    askNikeToken,
}