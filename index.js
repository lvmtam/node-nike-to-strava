'use strict';

const fs = require('fs');
const fse = require('fs-extra');
const cp = require('child_process');
const { AutoComplete, Confirm, prompt } = require('enquirer');
const strava = require('strava-v3');
const generate = require('./generate-strava-access-token')
const figlet = require('figlet');
const chalk = require('chalk');
const clear = require('clear');
const Configstore = require('configstore');
const pkg = require('./package.json')
const conf = new Configstore(pkg.name)
const CLI = require('clui')
const Spinner = CLI.Spinner

const STRAVA_CONFIG = './data/strava_config'
const EXPORT_PATH = './export';
const RUN_NAME_PREFIX = '(CLI.Swoosh)'

const { fetchRuns, fetchActivityDetail, testTokenValid } = require('./lib/nike-api')
const { askChoiceActivity, askNikeToken } = require('./lib/inquire')

  

/******************************************/
/********* MAIN SCRIPT ********************/
/******************************************/
async function main(nike_token) {

    clear()

    console.log(
        chalk.yellow(
            figlet.textSync('Nike 2 Strava', { horizontalLayout: 'full' })
        )
    )

    console.log(chalk.green('|=============================|'))
    console.log(chalk.green('|=== Fetch Nike+ Run Data ====|'))
    console.log(chalk.green('|=============================|'))
    console.log('')

    /********** GET USER INPUT **********/
    let nikeToken = nike_token
    if (!nikeToken) {
        nikeToken = conf.get('nike.token')
    }

    const valid = await testTokenValid(nikeToken)

    if (!valid) {
        nikeToken = await askNikeToken()
        if (nikeToken) {
            console.log(chalk.green('✅ updated new nike token in config file'))
            console.log('')
            conf.set('nike.token', nikeToken)
        }
    }

    if (!nikeToken) {
        console.log(chalk.red('ERROR. invalid token'))
        process.exit()
    }
    /************************************/
    
    
    const status = new Spinner('▶ Fetch your activities, please wait...', ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);
    status.start()

    fetchRuns(nikeToken)
    .then((runs) => {
        status.stop()
        return askChoiceActivity(runs)
    })
    .then((runId) => {
        status.start()
        return fetchActivityDetail(nikeToken, runId)
    })
    .then((nikeJson) => {
        status.message('▶ Converting your activity to GPX...')
        /********** CONVERT .JSON TO .GPX **********/
        const runName = nikeJson.tags['com.nike.name'] + ' ' + RUN_NAME_PREFIX
        const runDate = new Date(nikeJson.start_epoch_ms)
        const runStart = new Date(nikeJson.start_epoch_ms)
        const runEnd = new Date(nikeJson.end_epoch_ms)
        
        const elevations = nikeJson.metrics.find(metric => metric.type === 'elevation')
        const longitude = nikeJson.metrics.find(metric => metric.type === 'longitude')
        const latitude = nikeJson.metrics.find(metric => metric.type === 'latitude')
        const heartRate = nikeJson.metrics.find(metric => metric.type === 'heart_rate')

        if (!elevations || !longitude || !latitude || !heartRate) {
            console.log(chalk.yellow('The choiced run is lack of information to convert to .gpx file. Choose other one'))
            return
        }
        
        function getValueInTime(values, time, debug = false) {
            const epoch = Math.round(time.valueOf() / 1000)
            debug && console.log('epoch:', epoch)
            const found = values.find(value => {
                const start = Math.round(value.start_epoch_ms / 1000)
                const end = Math.round(value.end_epoch_ms / 1000)
                const valid = epoch >= start && epoch <= end
                debug && console.log(`${start} <= ${epoch} <= ${end} >>> ${valid ? 'OK' : '--'}`)
                return valid
            })
            if (found) {
                return found.value
            }
            return null
        }
        
        function setNextValue(obj, time, item, key, debug) {
            const { values } = obj
            const next = getValueInTime(values, time, debug)
            debug && console.log('next:', next)
            if (next) {
                item[key] = next
                debug && console.log(`set item[${key}] to:`, item[key])
            }
        }
        
        
        /***********************/
        /**** build GPX file ***/
        /***********************/
        const { buildGPX, GarminBuilder } = require('gpx-builder');
        const { Point, Metadata, Person, Track, Segment } = GarminBuilder.MODELS;
        
        const points = []
        const start = new Date(runStart)
        const params = {
            lon: 0,
            lat: 0,
            hr: 0,
            ele: 0,
        }
        
        while (start.getTime() <= runEnd.getTime()) {
            setNextValue(longitude, start, params, 'lon')
            setNextValue(latitude, start, params, 'lat')
            setNextValue(heartRate, start, params, 'hr')
            setNextValue(elevations, start, params, 'ele')
            // console.log('✅ a point at epoch:', start, '>>>', JSON.stringify(params))
            const { lat, lon, ele, hr } = params
            const point = new Point(lat, lon, {
                ele: ele,
                time: new Date(start),
                hr: hr,
            })
            points.push(point)
        
            start.setSeconds(start.getSeconds() + 1)
        }
        
        
        const person = new Person({
            name: 'lvmtam',
            email: 'levominhtam@gmail.com',
        })
        const metadata = new Metadata({
            name: runName,
            author: person,
            time: runStart,
        })
        const segment = new Segment(points)
        const track = new Track([segment], {
            name: runName,
            type: 9,
        })
        const gpxData = new GarminBuilder()
        gpxData.setMetadata(metadata)
        gpxData.setTracks([track])
        const gpxContent = buildGPX(gpxData.toObject())
        status.stop()

        fse.ensureDirSync(EXPORT_PATH)
        const filePath = `${EXPORT_PATH}/${Date.now()}.gpx`
        fs.writeFile(filePath, gpxContent, async function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("Converted .json to gpx at:", filePath);

            const prompt = new Confirm({
                type: "question",
                message: "Do you want to upload to Strava?",
                initial: 'n',
            })
            prompt
            .run()
            .then(async (yes) => {
                if (yes) {
                    console.log('|=====================================|')
                    console.log('|=== Step. upload to Strava ==========|')
                    console.log('|=====================================|')
                    if (!fs.existsSync(STRAVA_CONFIG)) {
                        await generate()
                    }

                    const content = fs.readFileSync(STRAVA_CONFIG)
                    const configs = JSON.parse(content)
                    console.log('strava configs:', configs)
                    if (!configs.access_token) {
                        await generate()
                    }

                    strava.uploads.post({
                        data_type: 'gpx',
                        file: filePath,
                        name: runName,
                    }, async (error, payload) => {
                        if (error && error.statusCode === 401) {
                            console.log('strava.uploads.post access_token invalid, will regenerate')
                            await generate()
                            strava.config(JSON.parse(fs.readFileSync(STRAVA_CONFIG)))
                            strava.uploads.post({
                                data_type: 'gpx',
                                file: filePath,
                                name: runName,
                            }, (e, p) => {
                                console.log('strava.uploads.post done. error.', e, p)
                            })
                        } else {
                            console.log('Step. upload to Strava - DONE')
                        }
                    })
                }
            })
        });
    })
    .catch((e) => {
        console.log('Seem to be token invalid or expired. please check again. error.', e.message)
    })
    .then(() => {
        status.stop()
    })
    
}

const args = process.argv.slice(2)
main(args[0])