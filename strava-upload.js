const strava = require('strava-v3')
const { createTerminus } = require('@godaddy/terminus')
const express = require('express')
const app = express()
const port = 3000

strava.config({
  "access_token": "59f784ad6c9230e4c1dfa31d6fba90f63b844792",
  "client_id": "45385",
  "client_secret": "5cf458aba39aa37b2086ba8cd85d8fb0ab064c37",
  "refresh_token": "52cbe0c66056e3fefb50ae7cac049d97505b0734",
  "redirect_uri": `http://localhost:${port}/exchange_token`,
})

/************************ MINI HTTP SERVER ************************/
app.use(express.static(__dirname + '/public'))

app.get('/exchange_token', async (req, res) => {
  const { code } = req.query
  console.log('Handled /exchange_token, code:', code)

  // TODO: get access_token by code
  const response = await strava.oauth.getToken(code)
  const accessToken = response.data.access_token
  console.log('strava/get_token grant:', accessToken)
  res.redirect(`/welcome.html?access_token=${accessToken}`)
})

function onSignal () {
  console.log('server is starting cleanup')
  // start cleanup of resource, like databases or file descriptors
}

function onShutdown () {
  console.log('server is starting shutdown')
  // start cleanup of resource, like databases or file descriptors
}

async function onHealthCheck () {
  // checks if the system is healthy, like the db connection is live
  // resolves, if health, rejects if not
}

createTerminus(app, {
  signal: 'SIGINT',
  healthChecks: { '/healthcheck': onHealthCheck },
  onSignal,
  onShutdown,
})

function startServer() {
  return app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })
}

function stopServer() {
  server.close(() => {
    console.log('Closed out remaining connections');
    process.exit(1)
  })
}


/************************ MAIN CODE ************************/

async function main() {
  startServer()
  const requestURL = strava.oauth.getRequestAccessURL({ scope: "activity:write" })
  console.log(`Open the following URL in a browser to continue: ${requestURL}\n`)
}


try {
  main()
} catch (e) {
  console.log('ERROR.', e)
  onSignal()
}